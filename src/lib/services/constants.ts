import BitcoinSvelte from '../icons/Bitcoin.svelte';
import BNBSvelte from '../icons/BNB.svelte';
import EthereumSvelte from '../icons/Ethereum.svelte';
import USDCSvelte from '../icons/USDC.svelte';
import USDTSvelte from '../icons/USDT.svelte';
import BCHSvelte from '../icons/BitcoinCash.svelte';
import LTCSvelte from '../icons/Litecoin.svelte';
import DOGESvelte from '../icons/Doge.svelte';
import AVAXSvelte from '../icons/Avalanche.svelte';
import ATOMSvelte from '../icons/CosmosHub.svelte';

export const assets = [
	{
		name: 'BTC',
		chain: 'Bitcoin',
		assetCode: 'BTC.BTC',
		swapURL: 'https://app.thorswap.finance/swap/BTC.BTC_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/BTC.BTC',
		color: 'text-orange',
		icon: BitcoinSvelte,
	},
	{
		name: 'ETH',
		chain: 'Ethereum',
		assetCode: 'ETH.ETH',
		swapURL: 'https://app.thorswap.finance/swap/ETH.ETH_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/ETH.ETH',
		color: 'text-slate-400',
		icon: EthereumSvelte,
	},
	{
		name: 'USDC',
		chain: 'USDC',
		assetCode: 'ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48',
		swapURL: 'https://app.thorswap.finance/swap/ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48',
		color: 'text-slate-400',
		icon: USDCSvelte,
	},
	{
	name: 'USDT',
		chain: 'Tether',
		assetCode: 'ETH.USDT-0XDAC17F958D2EE523A2206206994597C13D831EC7',
		swapURL: 'https://app.thorswap.finance/swap/ETH.USDT-0XDAC17F958D2EE523A2206206994597C13D831EC7_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/ETH.USDT-0XDAC17F958D2EE523A2206206994597C13D831EC7',
		color: 'text-slate-400',
		icon: USDTSvelte,
	},
	{
		name: 'BNB',
		chain: 'BNB Chain',
		assetCode: 'BSC.BNB',
		swapURL: 'https://app.thorswap.finance/swap/BSC.BNB_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/BSC.BNB',
		color: 'text-yellow-400',
		icon: BNBSvelte,
	},
	{
		name: 'BCH',
		chain: 'Bitcoin Cash',
		assetCode: 'BCH.BCH',
		swapURL: 'https://app.thorswap.finance/swap/BCH.BCH_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/BCH.BCH',
		color: 'text-emerald-500',
		icon: BCHSvelte,
	}, 
	{
		name: 'LTC',
		chain: 'Litecoin',
		assetCode: 'LTC.LTC',
		swapURL: 'https://app.thorswap.finance/swap/LTC.LTC_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/LTC.LTC',
		color: 'text-cyan-800',
		icon: LTCSvelte,
	},
	{
		name: 'DOGE',
		chain: 'Dogecoin',
		assetCode: 'DOGE.DOGE',
		swapURL: 'https://app.thorswap.finance/swap/DOGE.DOGE.RUNE',
		depositURL: 'https://app.thorswap.finance/add/DOGE.DOGE',
		color: "text-yellow-700",
		icon: DOGESvelte,
	},
	{
		name: 'AVAX',
		chain: 'Avalanche',
		assetCode: 'AVAX.AVAX',
		swapURL: 'https://app.thorswap.finance/swap/AVAX.AVAX_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/AVAX.AVAX',
		color: "text-rose-700",
		icon: AVAXSvelte,
	},
	{
		name: 'ATOM',
		chain: 'Cosmos Hub',
		assetCode: 'GAIA.ATOM',
		swapURL: 'https://app.thorswap.finance/swap/GAIA.ATOM_THOR.RUNE',
		depositURL: 'https://app.thorswap.finance/add/GAIA.ATOM',
		color: "text-indigo-900",
		icon: ATOMSvelte,
	},
];

export const totalSupply = 500000000;

export const appsAndServices = [
	{
		title: 'RuneScan',
		url: 'https://runescan.io',
		logo: '/images/services/viewblock.png',
		description:
			'RuneScan is a block explorer that lets users explore, filter, and query blocks confirmed on the THORChain network.',
		tags: ['Explorer', 'Dashboard']
	},
	{
		title: 'Thorchain Explorer',
		url: 'https://thorchain.net',
		logo: '/images/services/your-thorchad.png',
		description:
			'THORChain Block Explorer and Dashboard. Explore meaningful THORChain data in one simple interface.',
		tags: ['Explorer', 'Dashboard']
	},
	{
		title: 'Trust Wallet',
		url: 'https://trustwallet.com',
		logo: '/images/services/trustwallet.svg',
		description:
			'Trust Wallet is a self-custody wallet on iOS and Android that supports native RUNE and cross-chain swaps powered by THORChain.',
		tags: ['Wallet', 'RUNE', 'Swaps']
	},
	{
		title: 'THORSwap',
		url: 'https://app.thorswap.finance/',
		logo: '/images/services/thorswap.png',
		description:
			'THORSwap is the leading multi-chain decentralized exchange aggregator and flagship interface for all THORChain features. Perform cross-chain swaps between 5,500+ assets across 10+ blockchains in a single transaction. Powered by SwapKit.',
		tags: ['Swaps', 'Savers', 'Lending'],
	},
	{
		title: 'XDEFI',
		url: 'https://www.xdefi.io/',
		logo: '/images/services/xdefi.png',
		description:
			'XDEFI is a multichain wallet with native RUNE support that allows you to securely store, swap, and send Crypto and NFTs across dozens of blockchains including a variety of chains including UTXOs, EVMs, Cosmos chains, and more',
		tags: ['Wallet', 'RUNE', 'Swaps']
	},
	{
		title: 'ThorWallet',
		url: 'https://thorwallet.app.link/4MoUX7GFzmb',
		logo: '/images/services/thorwallet.svg',
		description:
			'THORWallet DEX is a mobile DeFi wallet. It\'s a non-custodial wallet with integrated cross-chain Dex. You can swap native tokens and manage your liquidity positions on THORChain, on/off ramp fiat, and more.',
		tags: ['Wallet', 'RUNE', 'Swaps']
	},
	{
		title: 'Rango Exchange',
		url: 'https://app.rango.exchange/swap/BTC.BTC/ETH.ETH',
		logo: '/images/services/rango.png',
		description:
			'Rango Exchange a powerful multi-chain aggregator for DEX and bridges, based on reachability and support of top blockchains. Rango will be able to find the most secure, fast, and easy path for it. Currently, Rango supports more than dozens of blockchains, 12+ bridges/DEXes, and 7+ different wallets, with a modern and user-friendly UX in the market.',
		tags: ['Swaps'],
	},
	{
		title: 'ASGARDEX',
		url: 'https://www.asgardex.com/installer',
		logo: '/images/services/asgardex.png',
		description:
			'ASGARDEX is a no-fee, standalone desktop app implementing THORChain-based decentralized swaps. As the only open-source frontend codebase, it serves as a reference for many developers building in the ecosystem.',
		tags: ['Wallet', 'RUNE', 'Swaps', 'Savers']
	},
	{
		title: 'ShapeShift',
		url: 'https://shapeshift.com/',
		logo: '/images/services/shapeshiftdao.svg',
		description:
			'ShapeShift is a pioneer in self-custody for digital asset trading. Our open source platform empowers users to safely buy, hold, trade, and invest in a range of digital assets and improves access to open, decentralized financial systems.',
		tags: ['Swaps', 'Savers', 'Lending', 'Wallet', 'RUNE']
	},
	{
		title: 'Li.Fi',
		url: 'https://li.fi/',
		logo: '/images/services/lifi.svg',
		description:
			'One API to swap, bridge, and zap across all major blockchains and protocols. Enable trading across all DEX aggregators, bridges, and intent-systems and save hundreds of developer hours.',
		tags: ['Swaps']
	},
	{
		title: 'Cake Wallet',
		url: 'https://shapeshift.com/',
		logo: '/images/services/cakewallet.png',
		description:
			'Cake Wallet is a non-custodial wallet for mobile and desktop devices. This means that all information about your wallet stores on your device, and you, the user, are 100% in control of your wallet and your funds.',
		tags: ['Swaps']
	},
	{
		title: 'Ledger',
		url: 'https://support.ledger.com/hc/en-us/articles/4402987997841-THORChain-RUNE-?support=true',
		logo: '/images/services/ledger.jpeg',
		description: 'Ledger is a popular multichain hardware wallet that supports native RUNE. Ledger is supported by many exchanges that use THORChain.',
		tags: ['Wallet', 'RUNE'],
	},
	{
		title: 'Edge',
		url: 'https://edge.app/',
		logo: '/images/services/edge.svg',
		description:
			'With an emphasis on privacy and security, Edge empowers users to buy, sell, store, trade, and stake various cryptocurrencies with peace of mind. Edge provides a user-friendly experience coupled with robust privacy features, making it an ideal choice for managing your digital assets. Utilizing SwapKit SDK.',
		tags: ['Wallet', 'RUNE', 'Swaps', 'Savers'],
	},
	{
		title: 'Lends',
		url: 'https://lends.so',
		logo: '/images/services/lends.svg',
		description:
			'Cross-chain lending solution powered by THORChain with access to the most competitive supply and borrowing rates through the Arbitrum ecosystem.',
		tags: ['Swaps', 'Savers', 'Lending'],
	},
	{
		title: 'KeepKey',
		url: 'https://www.keepkey.com/',
		logo: '/images/services/keepkey.png',
		description:
			'KeepKey is the leading hardware wallet for securely storing digital assets, including RUNE. Our Dapp Store offers access to thousands of decentralized finance opportunities. Get started today and unlock the world of secure, decentralized finance. Utilizing SwapKit SDK.',
		tags: ['Wallet', 'RUNE', 'Swaps'],
	},
	{
		title: 'OneKey',
		url: 'https://onekey.so/',
		logo: '/images/services/onekey.svg',
		description:
			'OneKey offers a simplified, comprehensive solution for managing and trading crypto asset, with 1m+ users. OneKey streamlines crypto asset management by consolidating various decentralized platforms into a single, secure, user-friendly interface. Utilizing SwapKit SDK.',
		tags: ['Wallet', 'Swaps'],
	},
	{
		title: 'Symbiosis',
		url: 'https://symbiosis.finance',
		logo: '/images/services/symbiosis.jpeg',
		description:
			'Symbiosis is a cross-chain AMM DEX that pools together liquidity from different networks: L1s and L2s, EVM and non-EVM. With Symbiosis, you can easily swap any token and move your assets across different networks.',
		tags: ['Swaps'],
	},
	{
		title: 'Rubic',
		url: 'https://rubic.exchange',
		logo: '/images/services/rubic.png',
		description:
			'Rubic aggregates swaps of 15,000+ assets across dozens of blockchains with the best rates, highest liquidity, and transaction speeds - in one click, thanks to the integration of 220+ DEXs and bridges. THORChain integration powered by Symbiosis.',
		tags: ['Swaps'],
	},
	{
		title: 'RocketX',
		url: 'https://rocketx.exchange',
		logo: '/images/services/rocketx.png',
		description:
			'RocketX is the most advanced hybrid aggregator that simplifies access to $100B+ liquidity across 300+ DEXs & 6 Top CEXs via a Single UI and API',
		tags: ['Swaps']
	},
	{
		title: 'Giddy',
		url: 'https://giddy.co/',
		logo: '/images/services/giddy.svg',
		description:
			'The self-custody, recoverable smart wallet that connects you to passive income earning opportunities',
		tags: ['Wallet', 'Swaps']
	},
	{
		title: 'Leap Wallet',
		url: 'https://www.leapwallet.io/download',
		logo: '/images/services/leapwallet.svg',
		description:
			'Leap Wallet is a multichain and cross-platform wallet supporting Cosmos and EVM chains, including native RUNE. Utilizing SwapKit.',
		tags: ['Wallet', 'RUNE']
	},
	{
		title: 'Coinbot',
		url: 'https://t.me/TeamCoinBot_bot',
		logo: '/images/services/coinbot.png',
		description:
			'Coinbot is a Telegram-based DEX bot that aims to bridge all chains and tokens, allowing users to perform any-to-any swaps.',
		tags: ['Swaps', 'RUNE']
	},
	{
		title: 'Unizen',
		url: 'https://unizen.io',
		logo: '/images/services/unizen.png',
		description:
			'Unizen is a cutting-edge operating system for Web3 applications that enables users to seamlessly, cost-efficiently, and securely interact with all things Web3, including THORChain-powered cross-chain swaps.',
		tags: ['Swaps']
	},
	{
		title: 'Swapper',
		url: 'https://app.swapper.market/',
		logo: '/images/services/swapper.svg',
		description:
			'Swapper enables everyone to buy, store, manage, invest, and transfer crypto effortlessly with seamless interaction with DeFi protocols across multiple blockchains. It aims to address various challenges, such as limited banking accessibility, distrust in centralized institutions, and mounting regulatory pressures faced by traditional industry players.',
		tags: ['Swaps']
	},

	{
		title: 'Defispot',
		url: 'https://defispot.com',
		logo: '/images/services/defispot.svg',
		description:
			'Defispot is an all in one multi-chain DEX built on THORChain, that brings usability to DeFi for all users by offering a traditional UI/UX- a DEX disguised as a CEX.',
		tags: ['Swaps']
	},
	{
		title: 'Liquality',
		url: 'https://liquality.io/',
		logo: '/images/services/liquality.svg',
		description: 'Liquality is a multichain wallet that uses THORChain for cross-chain swaps.',
		tags: ['Wallet', 'Swaps'],
	},
	{
		title: 'Decentralfi',
		url: 'https://decentralfi.io/',
		logo: '/images/services/decentralfi.svg',
		description:
			'Decentralfi is a decentralized exchange built with THORChain. Decentralfi has a slippage optimization bot named Slippy to prompt users to break up large trades to optimize swap fees.',
		tags: ['Swaps']
	},
	{
		title: 'Kensho Finance',
		url: 'https://www.kensho.finance/',
		logo: '/images/services/kensho.png',
		description:
			'Kensho Finance is a decentralized trading protocol that provides users with access to 9+ blockchains',
		tags: ['Swaps']
	},
	{
		title: 'Transaction Tracker',
		url: 'https://track.ninerealms.com',
		logo: '/images/services/ninerealms.png',
		description:
			'Open Source transaction tracker for THORChain Swaps, developed by Nine Realms',
		tags: ['Dashboard']
	},
	{
		title: 'Thorscanner',
		url: 'https://thorscanner.org',
		logo: '/images/services/thorscanner.png',
		description:
			'The THORChain block explorer designed for speed and efficiency.',
		tags: ['Explorer', 'Dashboard']
	},
	{
		title: 'Maya Protocol',
		url: 'https://www.mayaprotocol.com/',
		logo: '/images/services/maya.png',
		description:
			'Maya Protocol is a cross-chain (AMM) and friendly fork of Thorchain supporting different chains & assets as well as RUNE, expanding the reach of its technology and exchange pairs.',
		tags: ['Maya']
	},
	{
		title: 'THORMon Node Monitor',
		url: 'https://thornode.network/',
		logo: '/images/services/thormon.svg',
		description:
			'THORMon Node Dashboard - Track THORNodes, rewards, bond providers, and much more',
		tags: ['Dashboard']
	},
	{
		title: 'Liquity THORNode Monitor',
		url: 'https://thornode.network/',
		logo: '/images/services/your-thorchad.png',
		description:
			'THORChain Node Explorer and Information - Track node rewards, age, version, APRs, slashes, and more.',
		tags: ['Dashboard']
	},
	{
		title: 'Flipside Crypto',
		url: 'https://flipsidecrypto.xyz/',
		logo: '/images/services/flipside.jpeg',
		description:
			'Flipside crypto is a community of data analysts that publish data queries, dashboards, and datasets to answer questions that are important to protocols - including THORChain.',
		tags: ['Dashboard']
	},
	{
		title: 'THORCharts',
		url: 'https://thorcharts.org/',
		logo: '/images/services/thorcharts.svg',
		description:
			'THORCharts is a useful dashboard for THORChain-related statistics and analytics including RUNE price, volume, liquidity, and more.',
		tags: ['Dashboard']
	},
	{
		title: 'Nansen Portfolio',
		url: 'https://portfolio.nansen.ai/',
		logo: '/images/services/nansen.png',
		description:
			'Smart portfolio tracker with coverage of 476 protocols over 42 chains such as Ethereum, Thorchain, Cosmos, and more.',
		tags: ['Dashboard']
	},
	{
		title: 'Pulsar Finance',
		url: 'https://app.pulsar.finance/',
		logo: '/images/services/pulsar.svg',
		description:
			'Pulsar Finance is a useful dashboard to track your liquidity positions on THORChain and elsewhere.',
		tags: ['Dashboard']
	},
	{
		title: 'Odindex',
		url: 'https://odindex.io/',
		logo: '/images/services/odindex.svg',
		description:
			'Explore real-time THORChain data on token transfers between different chains and pools with an interactive map.',
		tags: ['Dashboard']
	},
	{
		title: 'Xchain.js',
		url: 'https://xchainjs.org',
		logo: '/images/services/xchainjs.svg',
		description:
			'XChainJS is a library with a common interface for multiple blockchains, built for simple and fast integration for wallets and more.',
		tags: ['Dev']
	},
	{
		title: 'SwapKit API+SDK',
		url: 'https://swapkit.dev',
		logo: '/images/services/swapkit.svg',
		description:
			'Swapkit’s SDK gives developers API access to a powerful suite of non-custodial, permissionless DeFi tools to interact with 5,500+ crypto assets across 10 blockchains including Bitcoin, Ethereum, and THORChain.',
		tags: ['Dev']
	},
	{
		title: 'THORChain Weekly Live RSS',
		url: 'https://rss.com/podcasts/thorchain/',
		logo: '/images/services/rss.png',
		description:
			'RSS Feed for THORChain Weekly Live, hosted by Nine Realms. Listen live on X or subscribe wherever you get your podcasts.',
		tags: ['Media']
	},
	{
		title: '0xVentures',
		url: 'https://0xventures.org',
		logo: '/images/services/0xventures.png',
		description:
			'0xVentures is a DAO that functions as a fund with the sole purpose of investing in sector-disrupting projects. We are users and creators who deeply care about integrating with blockchains and their communities.',
		tags: ['Community'],
	},
	{
		title: 'Caliber Capital',
		url: 'https://calibercapitalgroup.xyz/',
		logo: '/images/services/caliber.png',
		description:'Caliber Capital Group is a chain-agnostic marketing and consulting agency that specializes in bringing traditional marketing tools to web3,as well as tax services and legal counsel available for a consult.',
		tags: ['Community', 'NFT'],
	},
	{
		title: 'THORChain University',
		url: 'https://discord.gg/c4EhDZdFMA',
		logo: '/images/services/lp-university.png',
		description: 'Educational community discord server. Recommended to learn more about the basics of THORChain. Previously known as LP University.',
		tags: ['Community', 'Support'],
	},
	{
		title: 'THORChads DAO',
		url: 'https://thorchads.com/',
		logo: '/images/services/thorchad.png',
		description:
			'The rewards programme and home of the THORChads Metaverse, where the community can enjoy creative projects such as NFTs, Merch drops, IDO access and games. THORChads.com acts as a creative launchpad, supporting new THORChain community projects.',
		tags: ['NFT', 'Community', 'Launchpad'],
	},
	{
		title: 'Nine Realms',
		url: 'https://twitter.com/ninerealms_cap',
		logo: '/images/services/ninerealms.png',
		description:
			'Secure node operations and core dev for THORChain. Providing security, engineering, & integrations support to the THORChain community',
		tags: ['Dev','Security']
	},
	{
		title: 'Immunefi',
		url: 'https://immunefi.com/bounty/thorchain/',
		logo: '/images/services/immunefi.svg',
		description:
			'Immunefi is the 3rd party bug bounty provider for THORChain. Report vulnerabilities and receive rewards for responsible disclosure.',
		tags: ['Security']
	},
	{
		title: 'ThorGuards',
		url: 'https://thorguards.com/',
		logo: '/images/services/thorguards.png',
		description:
			'ThorGuards is the community hub of THORChain, building off of ecosystem partners and including them in the traits. The 3D art features a unique fusion of Norse mythology and Viking elements with cyberpunk aesthetics and accents.',
		tags: ['NFT', 'Community'],
	},
	{
		title: 'Qi Capital',
		url: 'https://www.qicapital.org',
		logo: '/images/services/qicapital.png',
		description:
			'Qi Capital is a tightly knit community of experienced crypto investors and traders who work together to discover new gems and trends and help young teams grow and thrive.',
		tags: ['Community']
	},
	{
		title: 'THORChain Alerts Telegram',
		url: 'https://t.me/thorchain_alert',
		logo: '/images/services/tcalert.jpeg',
		description:
			'Telegram bot to monitor major events on THORChain.',
		tags: ['Alerts', 'Dashboard']
	},
	{
		title: 'THORChain Infobot',
		url: 'https://t.me/thor_infobot',
		logo: '/images/services/tcalert.jpeg',
		description:
			'Telegram bot to receive alerts for your RUNE addresses, bond provision, LP or Savers positions, and more',
		tags: ['Alerts', 'Dashboard']
	},
	{
		title: 'THORChad Yourself',
		url: 'https://thorchad.glitch.me',
		logo: '/images/services/your-thorchad.png',
		description:
			'A simple bot to add the green THORChad ring around your profile picture for twitter, discord, or telegram.',
		tags: ['Other']
	},
	{
		title: 'GrassRoots Crypto',
		url: 'https://youtube.com/c/GrassRootsCrypto/',
		logo: '/images/services/grc.jpeg',
		description:
			'GrassRoots Crypto provides education services in the Blockchain and Crypto space via YouTube. Currently focusing on THORChain brining understanding to one of the most complex Defi Projects.',
		tags: ['Media']
	},
	{
		title: 'THORYield',
		url: 'https://app.thoryield.com/dashboard',
		logo: '/images/services/thoryield.jpeg',
		description:
			'With THORYield app you can Track your Liquidity on THORChain, view your wallet balances and staking accounts.',
		tags: ['Dashboard']
	},
	{
		title: 'Thorstarter',
		url: 'https://thorstarter.org',
		logo: '/images/services/thorstarter.svg',
		description:
			'Thorstarter is a multichain Venture DAO and IDO platform that combines a unique launchpad model with liquidity grants to incubate, fund, and launch the most promising projects across DeFi.',
		tags: ['Launchpad']
	},
	{
		title: 'THORSwap Discord',
		url: 'https://discord.gg/thorswap',
		logo: '/images/services/discord.svg',
		description:
			'THORSwap Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'XDEFI Discord',
		url: 'http://discord.gg/xdefi',
		logo: '/images/services/discord.svg',
		description:
			'XDEFI Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'THORWallet Discord',
		url: 'https://discord.gg/NkkyVayNy6',
		logo: '/images/services/discord.svg',
		description:
			'THORWallet Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'Defispot Discord',
		url: 'https://discord.gg/nEwKHrTJWc',
		logo: '/images/services/discord.svg',
		description:
			'Defispot Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'Rango Exchange Discord',
		url: 'https://discord.com/invite/q3EngGyTrZ',
		logo: '/images/services/discord.svg',
		description:
			'Rango Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'ShapeShift Discord',
		url: 'https://discord.com/invite/shapeshift',
		logo: '/images/services/discord.svg',
		description:
			'ShapeShift Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	},
	{
		title: 'Lends Discord',
		url: 'https://discord.gg/lends',
		logo: '/images/services/discord.svg',
		description:
			'ShapeShift Discord with a Help Desk for dedicated support',
		tags: ['Support', 'Community']
	}
];

export const updates = [
	{

        url: 'https://www.youtube.com/watch?v=b3OhOzvTKv4',

        title: 'How To Get THORChain Information & Dev Updates',

        thumbnail: '/images/media/thorchaininfo.jpeg'

    },

	{

        url: 'https://www.youtube.com/watch?v=CZLWT2lyiRU',

        title: 'Core Developer Explains How THORChain Works',

        thumbnail: '/images/media/edgediscussion.png'

    },
	
	{

        url: 'https://www.youtube.com/watch?v=gkNBl5xKfqQ',

        title: 'Cross Chain Panel: Quantum Miami 2023',

        thumbnail: '/images/media/quantum2023.jpeg'

    },

    {

        url: 'https://www.youtube.com/watch?v=vFQs8ML4qWE',

        title: 'Charlie Shrem: Untold Stories with Chad Barraford & Erik Voorhees',

        thumbnail: '/images/media/untold.png'

    },

	{
		url: 'https://youtube.com/playlist?list=PLMQ_o57NED-eh9eWxD-qqIvmdgXmW-6eX',

        title: 'THORChain Weekly Update Twitter Spaces',

        thumbnail: '/images/media/spaces.png'

    },

    {

        url: 'https://crypto101podcast.com/podcasts/ep-492-the-history-of-thorchain-a-bullish-case-for-regulation/',

        title: 'Crypto 101 Podcast: The History of THORChain',

        thumbnail: '/images/media/crypto101chad.png'

    },

    {

        url: 'https://crypto101podcast.com/podcasts/ep-439-executing-true-financial-inclusion-with-marcel-harmann-of-thorwallet-dex/',

        title: 'Crypto 101 Podcast: Marcel Harmann',

        thumbnail: '/images/media/crypto101marcel.png'

    },

    {

        url: 'https://bcdialogues.com/2023/01/01/ep-49-interview-chad-barraford-technical-lead-thorchain/',

        title: 'Blockchain Dialogues Episode #49: Chad Barraford',

        thumbnail: '/images/media/bcdialogue.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=pv_MS9DZz1s',

        title: 'Edge Wallet and THORChain: Savers and DEX Aggregation',

        thumbnail: '/images/media/thorchainedgespace.png'

    },

    {

        url: 'https://smarteconomypodcast.com/episode/db6986b8/chad-barraford-thorchain-empowering-users-through-decentralized-finance',

        title: 'Smart Economy Podcast: Chad Barraford',

        thumbnail: '/images/media/smarteconomy.png'

    },

    {

        url: 'https://cointelegraph.com/news/bitcoin-should-become-the-foundation-for-defi',

        title: 'Bitcoin could become the foundation of DeFi',

        thumbnail: '/images/media/bitcoinfoundation.png'

    },

    {

        url: 'https://financialit.net/news/cryptocurrencies/thorchain-enables-defi-bitcoin-breakthrough-single-sided-staking-service',

        title: 'THORChain Enables DeFi on Bitcoin',

        thumbnail: '/images/media/financialit.png'

    },

	{

        url: 'https://medium.com/thorchain/trust-wallet-integrates-thorchain-as-cross-chain-swap-provider-d56e01f1c146',

        title: 'Trust Wallet Integrates THORChain',

        thumbnail: '/images/media/trustwallet.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=1r9lG5D6Q6I',

        title: 'Digital Cash Network Podcast: Chad Barraford',

        thumbnail: '/images/media/digitalcashnetwork.png'

    },

    {

        url: 'https://blockworks.co/news/native-bitcoin-in-defi-this-dex-wants-to-boost-trust',

        title: 'Native Bitcoin in DeFi',

        thumbnail: '/images/media/blockworks1.png'

    },

    {

        url: 'https://www.crowdfundinsider.com/2022/12/200068-thorchain-integrates-with-trust-wallet-to-accelerate-adoption-of-crypto-self-custody/',

        title: 'THORChain Integrates with Trust Wallet',

        thumbnail: '/images/media/cfi.png'

    },

    {

        url: 'https://www.crowdfundinsider.com/2022/10/197130-thorchain-integrates-with-avalanche-to-support-multichain-interoperability/',

        title: 'THORChain Integrates with Avalanche',

        thumbnail: '/images/media/cfi2.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=5ClzXZN9fDM',

        title: 'CryptoCoinShow: Marcel Harmann, Founder of THORWallet DEX',

        thumbnail: '/images/media/ccshow.png'

    },

    {

        url: 'https://appdevelopermagazine.com/thorwallet-noncustodial-defi-wallet-review-with-marcel-harmann/',

        title: 'THORWallet review with Marcel Harmann',

        thumbnail: '/images/media/appdevmag.png'

    },

    {

        url: 'https://www.youtube.com/watch?v=CgYpsfCNwiE',

        title: 'Bridging the Gap Among Blockchains',

        thumbnail: '/images/media/thegap.png'

    },  


    {

        url: 'https://medium.com/thorchain/thorchain-savers-vaults-fc3f086b4057',

        title: 'Savers Vaults Live on THORChain',

        thumbnail: '/images/media/savers.png'

    },

    {

        url: 'https://medium.com/thorchain/thorchain-integration-of-avalanche-c-chain-complete-de8786ac7435',

        title: 'Integration of Avalanche C-Chain Complete',

        thumbnail: '/images/media/avax.png'

    },

    {

        url: 'https://medium.com/thorchain/edge-wallet-integrates-thorchain-f85a69f7e8e6',

        title: 'Edge Wallet Integrates THORChain',

        thumbnail: '/images/media/edgetc.png'

    },

    {

        url: 'https://medium.com/thorchain/thorchain-tokenomics-what-is-rune-52d339633260',

        title: 'THORChain Tokenomics — What is RUNE?',

        thumbnail: '/images/media/tokenomics.png'

    },

    {

        url: 'https://medium.com/thorchain/atom-trading-live-on-thorchain-mainnet-7d13d5d8544f',

        title: 'ATOM Live on THORChain Mainnet',

        thumbnail: '/images/media/atom.png'

    },

    {

        url: 'https://medium.com/thorchain/thorchains-layers-of-security-e308d537acf1',

        title: 'THORChain Layers of Security',

        thumbnail: '/images/media/security.png'

    },

    {

        url: 'https://www.coindesk.com/video/recent-videos/consensus-2022-foundations-thorchain/',

        title: 'Consensus 2022 Foundations: THORchain',

        thumbnail: '/images/media/consensus2022.png'

    },

    {

        url: 'https://www.binance.com/hi/live/video?roomId=2104136',

        title: 'Binance Live: Mainnet Achievement AMA',

        thumbnail: '/images/media/binancelive.png'

	}
];

export const devtools = [

	{

        url: 'https://docs.thorchain.org/',

        title: 'THORChain Network Documentation',

        thumbnail: '/images/media/mainnet.jpeg'

    },

	{

        url: 'https://www.youtube.com/watch?v=Qowrasst2UQ',

        title: 'Develop on THORChain - Video Guide',

        thumbnail: '/images/media/developguide.jpeg'

    },

	{

        url: 'https://discord.gg/tW64BraTnX',

        title: 'THORChain Developer Discord',

        thumbnail: '/images/media/discord.png'

    },

	{

        url: 'https://dev.thorchain.org',

        title: 'THORChain Developer Docs + Quickstart Guides',

        thumbnail: '/images/media/mainnet.jpeg'

    },

	{

        url: 'https://xchainjs.org',

        title: 'XChainJS - Multichain Library',

        thumbnail: '/images/media/xchainjs.png'

    },

];